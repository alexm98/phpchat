**PHPChat** is a web based chat written using PHP and Jquery Ajax to make it interactive. 

Here's a screenshot of what it looks like
![alt tag](http://s18.postimg.org/wezycvg15/2016_09_24_010704_1366x768_scrot.png)

##It's main features are:
* Grabbing new messages each second
* Deleting older messages
* Detecting and adding a link when it's pasted into the message box
* Detecting and adding an image when it's pasted into the message (supports .jpeg, .jpg and .png )
* The only external library used is Jquery, so it's pretty lightweight
