<?php include('conn.php'); ?>
<html>
<head>
        <title>Chat</title> 
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="utf-8">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> 
<script>
    //send messages
    function send_message(){
        var name = $('#user').val();
        var messg = $('#mess').val();

        if(name && messg){
            $('#user').hide('Slow');
            $('#userplh').hide('Slow');    
            $('.bar').show('Slow');
            $('.usn').html(name);

        //if the message has an image or a gif
        if(messg.includes('.png') || messg.includes('jpg') || messg.includes('jpeg') || messg.includes('gif')){
            var messg = '<img src="'+ messg + '"/>';
        }
        //or if the message is a link 
        else if(messg.includes('http:') || messg.includes('https:') || messg.includes('www.')){
            var messg = '<a href="'+ messg + '" target=_blank>'+ messg +'</a>';
            }
        $('#mess').val('');
        
        $.post('sendmsg.php',{user:name,mess:messg},
            function(data){
                $('#msg_display').append(data);
        });   
        }

    }
    // display the messages in a 1 second interval
    function show_message(){
        $.post('getmsg.php',{},
            function(getmessages){
                $('#msg_display').html(getmessages);
                $('#msg_display').scrollTop($('#msg_display').height());    
        });
    }
    setInterval(show_message,1000);
    
    // check and delete messages
    function delete_message(){
        $.post('delmsg.php');
    };
    setInterval(delete_message,1000);
    
    $(window).keyup(function (e) {
        if (e.keyCode == 13){ 
            send_message();                            
        }
    });

</script>
</head>
<body onload="show_message()">
    <div class="col-md-13" id="msg_display"></div> 
    <table class="messagebox">
    <tr>
        <td>
            <label id="userplh">Username :</label>
            <input id="user" type="text" placeholder=" Username"/>
        </td>
    </tr> 
    <tr>
        <td>
            <textarea id="mess" rows="4" cols="50"></textarea>
            <table class="bar">
                <tr> 
                    <td><div class="usn"></div></td>
                </tr>
            </table>
        </td>
        <td>
            <button onclick="send_message()">Send</button>
        </td>
    </tr>
    </table> 
</body>
</html>
