<?php include("conn.php");

// grab the max id 
$query = "SELECT MAX(id) as maxid FROM mesaje";
$maxid = mysqli_query($conn,$query);

// delete rows under a specific id
$del = "DELETE FROM mesaje WHERE id < 100";
    
while($row = mysqli_fetch_array($maxid)){
    echo $row['maxid'];
    if($row['maxid'] >= 100){
        $delete = mysqli_query($conn,$del);
        // delete the id column for resetting the delete procedure
        $delid = mysqli_query($conn,"ALTER TABLE mesaje DROP id");
        $insertid = mysqli_query($conn,"ALTER TABLE `mesaje` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`)");
    }
}

mysqli_close($conn);
?>
